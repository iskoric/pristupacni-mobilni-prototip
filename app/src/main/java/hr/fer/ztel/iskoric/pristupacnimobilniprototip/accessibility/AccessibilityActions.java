package hr.fer.ztel.iskoric.pristupacnimobilniprototip.accessibility;

import android.view.View;

public class AccessibilityActions {

    private final AccessibilityPreferences preferences;
    private final AccessibilityUpdate update;

    private final View mainView;

    public AccessibilityActions(AccessibilityUpdate update, View mainView) {
        this.preferences = update.preferences;
        this.update = update;
        this.mainView = mainView;
    }

    public void increaseFontSize() {
        if (preferences.getFontSizeXS() < 29) {
            preferences.setFontSizeXS(preferences.getFontSizeXS() * 1.2f);
        }
        if (preferences.getFontSizeS() < 33) {
            preferences.setFontSizeS(preferences.getFontSizeS() * 1.2f);
        }
        if (preferences.getFontSizeM() < 37) {
            preferences.setFontSizeM(preferences.getFontSizeM() * 1.2f);
        }
        if (preferences.getFontSizeL() < 41) {
            preferences.setFontSizeL(preferences.getFontSizeL() * 1.2f);
        }
        if (preferences.getFontSizeXL() < 49) {
            preferences.setFontSizeXL(preferences.getFontSizeXL() * 1.2f);
        }
        if (preferences.getLogoSize() < 66) {
            preferences.setLogoSize(preferences.getLogoSize() * 1.2f);
        }
        update.updateMain(mainView);
    }

    public void decreaseFontSize() {
        if (preferences.getFontSizeXS() > 10) {
            preferences.setFontSizeXS(preferences.getFontSizeXS() / 1.2f);
        }
        if (preferences.getFontSizeS() > 12) {
            preferences.setFontSizeS(preferences.getFontSizeS() / 1.2f);
        }
        if (preferences.getFontSizeM() > 13) {
            preferences.setFontSizeM(preferences.getFontSizeM() / 1.2f);
        }
        if (preferences.getFontSizeL() > 14) {
            preferences.setFontSizeL(preferences.getFontSizeL() / 1.2f);
        }
        if (preferences.getFontSizeXL() > 17) {
            preferences.setFontSizeXL(preferences.getFontSizeXL() / 1.2f);
        }
        if (preferences.getLogoSize() > 23) {
            preferences.setLogoSize(preferences.getLogoSize() / 1.2f);
        }
        update.updateMain(mainView);
    }

    public void resetFontSize() {
        preferences.setFontSizeXS(AccessibilityPreferences.fontSizeXS);
        preferences.setFontSizeS(AccessibilityPreferences.fontSizeS);
        preferences.setFontSizeM(AccessibilityPreferences.fontSizeM);
        preferences.setFontSizeL(AccessibilityPreferences.fontSizeL);
        preferences.setFontSizeXL(AccessibilityPreferences.fontSizeXL);

        preferences.setLogoSize(AccessibilityPreferences.logoSize);

        update.updateMain(mainView);
    }

    public void toggleDyslexicFont() {
        preferences.setUseDyslexic(!preferences.getUseDyslexic());

        update.updateMain(mainView);
    }

    public void setContrast(ContrastEnum contrast) {
        preferences.setContrast(contrast);

        update.updateMain(mainView);
    }

    public void toggleUnderlineLinks() {
        preferences.setUnderlineLinks(!preferences.getUnderlineLinks());

        update.updateMain(mainView);
    }

    public void toggleHighlightLinks() {
        preferences.setHighlightLinks(!preferences.getHighlightLinks());

        update.updateMain(mainView);
    }

    public void toggleGrayscale() {
        preferences.setUseGrayScale(!preferences.getUseGrayScale());

        update.updateMain(mainView);
    }

    public void toggleNightMode() {
        preferences.setUseNightMode(!preferences.getUseNightMode());

        update.updateMain(mainView);
    }

    public void resetSettings() {
        preferences.setFontSizeXS(AccessibilityPreferences.fontSizeXS);
        preferences.setFontSizeS(AccessibilityPreferences.fontSizeS);
        preferences.setFontSizeM(AccessibilityPreferences.fontSizeM);
        preferences.setFontSizeL(AccessibilityPreferences.fontSizeL);
        preferences.setFontSizeXL(AccessibilityPreferences.fontSizeXL);

        preferences.setLogoSize(AccessibilityPreferences.logoSize);

        preferences.setUseDyslexic(AccessibilityPreferences.useDyslexic);

        preferences.setContrast(AccessibilityPreferences.contrast);

        preferences.setUnderlineLinks(AccessibilityPreferences.underlineLinks);
        preferences.setHighlightLinks(AccessibilityPreferences.highlightLinks);

        preferences.setUseGrayScale(AccessibilityPreferences.useGrayscale);

        preferences.setUseNightMode(AccessibilityPreferences.useNightMode);

        update.updateMain(mainView);
    }
}
