package hr.fer.ztel.iskoric.pristupacnimobilniprototip.accessibility;

import android.content.Context;
import android.content.SharedPreferences;

import hr.fer.ztel.iskoric.pristupacnimobilniprototip.R;

public class AccessibilityPreferences {

    public static final float fontSizeXS = 14;
    public static final float fontSizeS = 16;
    public static final float fontSizeM = 18;
    public static final float fontSizeL = 20;
    public static final float fontSizeXL = 24;

    public static final float logoSize = 32;

    public static final boolean useDyslexic = false;

    public static final ContrastEnum contrast = ContrastEnum.NONE;

    public static final boolean underlineLinks = false;
    public static final boolean highlightLinks = false;

    public static final boolean useGrayscale = false;

    public static final boolean useNightMode = false;

    public final Context context;

    private final SharedPreferences preferences;

    public AccessibilityPreferences(Context context) {
        this.context = context;
        this.preferences = context.getSharedPreferences(
                context.getString(R.string.accessibility_preferences),
                Context.MODE_PRIVATE
        );
    }

    public float getFontSizeXS() {
        return preferences.getFloat(context.getString(R.string.pref_font_size_xs), fontSizeXS);
    }

    public void setFontSizeXS(float size) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(context.getString(R.string.pref_font_size_xs), size);
        editor.apply();
    }

    public float getFontSizeS() {
        return preferences.getFloat(context.getString(R.string.pref_font_size_s), fontSizeS);
    }

    public void setFontSizeS(float size) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(context.getString(R.string.pref_font_size_s), size);
        editor.apply();
    }

    public float getFontSizeM() {
        return preferences.getFloat(context.getString(R.string.pref_font_size_m), fontSizeM);
    }

    public void setFontSizeM(float size) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(context.getString(R.string.pref_font_size_m), size);
        editor.apply();
    }

    public float getFontSizeL() {
        return preferences.getFloat(context.getString(R.string.pref_font_size_l), fontSizeL);
    }

    public void setFontSizeL(float size) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(context.getString(R.string.pref_font_size_l), size);
        editor.apply();
    }

    public float getFontSizeXL() {
        return preferences.getFloat(context.getString(R.string.pref_font_size_xl), fontSizeXL);
    }

    public void setFontSizeXL(float size) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(context.getString(R.string.pref_font_size_xl), size);
        editor.apply();
    }

    public float getLogoSize() {
        return preferences.getFloat(context.getString(R.string.pref_logo_size), logoSize);
    }

    public void setLogoSize(float size) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(context.getString(R.string.pref_logo_size), size);
        editor.apply();
    }

    public boolean getUseDyslexic() {
        return preferences.getBoolean(context.getString(R.string.pref_use_dyslexic), useDyslexic);
    }

    public void setUseDyslexic(boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(context.getString(R.string.pref_use_dyslexic), value);
        editor.apply();
    }

    public ContrastEnum getContrast() {
        int i = preferences.getInt(context.getString(R.string.pref_contrast), contrast.ordinal());
        return ContrastEnum.values()[i];
    }

    public void setContrast(ContrastEnum contrast) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(context.getString(R.string.pref_contrast), contrast.ordinal());
        editor.apply();
    }

    public boolean getUnderlineLinks() {
        return preferences.getBoolean(context.getString(R.string.pref_underline_links), underlineLinks);
    }

    public void setUnderlineLinks(boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(context.getString(R.string.pref_underline_links), value);
        editor.apply();
    }

    public boolean getHighlightLinks() {
        return preferences.getBoolean(context.getString(R.string.pref_highlight_links), highlightLinks);
    }

    public void setHighlightLinks(boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(context.getString(R.string.pref_highlight_links), value);
        editor.apply();
    }

    public boolean getUseGrayScale() {
        return preferences.getBoolean(context.getString(R.string.pref_use_grayscale), useGrayscale);
    }

    public void setUseGrayScale(boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(context.getString(R.string.pref_use_grayscale), value);
        editor.apply();
    }

    public boolean getUseNightMode() {
        return preferences.getBoolean(context.getString(R.string.pref_use_night_mode), useNightMode);
    }

    public void setUseNightMode(boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(context.getString(R.string.pref_use_night_mode), value);
        editor.apply();
    }
}
