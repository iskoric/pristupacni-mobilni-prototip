package hr.fer.ztel.iskoric.pristupacnimobilniprototip;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import hr.fer.ztel.iskoric.pristupacnimobilniprototip.accessibility.AccessibilityActions;
import hr.fer.ztel.iskoric.pristupacnimobilniprototip.accessibility.AccessibilityPreferences;
import hr.fer.ztel.iskoric.pristupacnimobilniprototip.accessibility.AccessibilityUpdate;
import hr.fer.ztel.iskoric.pristupacnimobilniprototip.accessibility.ContrastEnum;
import hr.fer.ztel.iskoric.pristupacnimobilniprototip.pages.PageEnum;
import hr.fer.ztel.iskoric.pristupacnimobilniprototip.pages.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity {

    private final PageEnum STARTING_PAGE = PageEnum.POCETNA;

    public AccessibilityUpdate update;

    private DrawerLayout drawer;
    private ViewPager viewPager;

    private AccessibilityPreferences preferences;
    private AccessibilityActions actions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View mainView = findViewById(R.id.main_view);
        drawer = findViewById(R.id.drawer_layout);
        viewPager = findViewById(R.id.view_pager);

        preferences = new AccessibilityPreferences(this);
        update = new AccessibilityUpdate(preferences);
        actions = new AccessibilityActions(update, mainView);

        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.START);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TabLayout pageIndicator = findViewById(R.id.page_indicator);

        for (PageEnum page : PageEnum.values()) {
            TabLayout.Tab tab = pageIndicator.newTab();
            tab.setText("");
            tab.setContentDescription(MainActivity.this.getString(page.getDescriptionId()));
            pageIndicator.addTab(tab);
        }

        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        viewPager.addOnPageChangeListener(
                new TabLayout.TabLayoutOnPageChangeListener(pageIndicator)
        );

        pageIndicator.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                viewPager.sendAccessibilityEvent(
                        AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED
                );
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        viewPager.setCurrentItem(STARTING_PAGE.ordinal());

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View view, float v) {
                if (view.getId() == R.id.navigation_view) {
                    ImageButton imageButton = findViewById(R.id.navigation_button);
                    if (v >= 0.5) {
                        if (!imageButton.getDrawable().equals(ContextCompat.getDrawable(view.getContext(), R.drawable.ic_arrow_back))) {
                            imageButton.setImageDrawable(ContextCompat.getDrawable(view.getContext(), R.drawable.ic_arrow_back));
                            imageButton.setContentDescription(MainActivity.this.getString(R.string.desc_close_navigation_menu));
                        }
                    } else {
                        if (!imageButton.getDrawable().equals(ContextCompat.getDrawable(view.getContext(), R.drawable.ic_menu))) {
                            imageButton.setImageDrawable(ContextCompat.getDrawable(view.getContext(), R.drawable.ic_menu));
                            imageButton.setContentDescription(MainActivity.this.getString(R.string.desc_open_navigation_menu));
                        }
                    }
                } else if (view.getId() == R.id.accessibility_view) {
                    ImageButton imageButton = findViewById(R.id.accessibility_button);
                    if (v >= 0.5) {
                        if (!imageButton.getDrawable().equals(ContextCompat.getDrawable(view.getContext(), R.drawable.ic_close))) {
                            imageButton.setImageDrawable(ContextCompat.getDrawable(view.getContext(), R.drawable.ic_close));
                            imageButton.setContentDescription(MainActivity.this.getString(R.string.desc_close_accessibility_menu));
                        }
                    } else {
                        if (!imageButton.getDrawable().equals(ContextCompat.getDrawable(view.getContext(), R.drawable.ic_accessibility))) {
                            imageButton.setImageDrawable(ContextCompat.getDrawable(view.getContext(), R.drawable.ic_accessibility));
                            imageButton.setContentDescription(MainActivity.this.getString(R.string.desc_open_accessibility_menu));
                        }
                    }
                }
            }

            @Override
            public void onDrawerOpened(@NonNull View view) {
            }

            @Override
            public void onDrawerClosed(@NonNull View view) {
            }

            @Override
            public void onDrawerStateChanged(int i) {
            }
        });

        RadioGroup contrastGroup = findViewById(R.id.contrast_group);
        contrastGroup.setOnCheckedChangeListener((group, checkedId) -> {
            for (int i = 0; i < group.getChildCount(); i++) {
                ToggleButton button = (ToggleButton) group.getChildAt(i);
                button.setChecked(button.getId() == checkedId);
            }
        });

        loadToggleButtonStates();
        update.updateMain(mainView);
    }

    public void openNavigationMenu(View view) {
        if (drawer.isDrawerVisible(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            }
            drawer.openDrawer(GravityCompat.START);
        }
    }

    public void openPagePocetna(View view) {
        viewPager.setCurrentItem(PageEnum.POCETNA.ordinal());
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openPageMladi(View view) {
        viewPager.setCurrentItem(PageEnum.MLADI.ordinal());
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openPageOSI(View view) {
        viewPager.setCurrentItem(PageEnum.OSI.ordinal());
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openPageSeniori(View view) {
        viewPager.setCurrentItem(PageEnum.SENIORI.ordinal());
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openPageKontakti(View view) {
        viewPager.setCurrentItem(PageEnum.KONTAKTI.ordinal());
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openPageInfo(View view) {
        viewPager.setCurrentItem(PageEnum.INFO.ordinal());
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openAccessibilityMenu(View view) {
        if (drawer.isDrawerVisible(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            }
            drawer.openDrawer(GravityCompat.END);
        }
    }

    public void increaseFontSize(View view) {
        actions.increaseFontSize();
    }

    public void decreaseFontSize(View view) {
        actions.decreaseFontSize();
    }

    public void resetFontSize(View view) {
        actions.resetFontSize();
    }

    public void dyslexicFont(View view) {
        actions.toggleDyslexicFont();
    }

    @SuppressLint("NonConstantResourceId")
    public void contrastChanged(View view) {
        ((RadioGroup) view.getParent()).check(view.getId());
        switch (view.getId()) {
            case R.id.contrast_none:
                actions.setContrast(ContrastEnum.NONE);
                break;
            case R.id.contrast_black:
                actions.setContrast(ContrastEnum.BLACK);
                break;
            case R.id.contrast_red:
                actions.setContrast(ContrastEnum.RED);
                break;
            case R.id.contrast_green:
                actions.setContrast(ContrastEnum.GREEN);
                break;
            case R.id.contrast_blue:
                actions.setContrast(ContrastEnum.BLUE);
                break;
        }
        ((ToggleButton) view).setChecked(true);
    }

    public void underlineLinks(View view) {
        actions.toggleUnderlineLinks();
    }

    public void highlightLinks(View view) {
        actions.toggleHighlightLinks();
    }

    public void grayscale(View view) {
        actions.toggleGrayscale();
    }

    public void nightMode(View view) {
        actions.toggleNightMode();
    }

    public void reset(View view) {
        ((ToggleButton) findViewById(R.id.dyslexic_font)).setChecked(false);
        contrastChanged(findViewById(R.id.contrast_none));
        ((ToggleButton) findViewById(R.id.underline_links)).setChecked(false);
        ((ToggleButton) findViewById(R.id.highlight_links)).setChecked(false);
        ((ToggleButton) findViewById(R.id.grayscale)).setChecked(false);
        ((ToggleButton) findViewById(R.id.night_mode)).setChecked(false);

        actions.resetSettings();
    }

    public void loadToggleButtonStates() {
        ((ToggleButton) findViewById(R.id.dyslexic_font)).setChecked(preferences.getUseDyslexic());
        ContrastEnum contrast = preferences.getContrast();
        switch (contrast) {
            case BLACK:
                contrastChanged(findViewById(R.id.contrast_black));
                break;
            case RED:
                contrastChanged(findViewById(R.id.contrast_red));
                break;
            case GREEN:
                contrastChanged(findViewById(R.id.contrast_green));
                break;
            case BLUE:
                contrastChanged(findViewById(R.id.contrast_blue));
                break;
            default:
                contrastChanged(findViewById(R.id.contrast_none));
                break;
        }
        ((ToggleButton) findViewById(R.id.underline_links)).setChecked(preferences.getUnderlineLinks());
        ((ToggleButton) findViewById(R.id.highlight_links)).setChecked(preferences.getHighlightLinks());
        ((ToggleButton) findViewById(R.id.grayscale)).setChecked(preferences.getUseGrayScale());
        ((ToggleButton) findViewById(R.id.night_mode)).setChecked(preferences.getUseNightMode());
    }

    @Override
    public void onBackPressed() {
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }
}
