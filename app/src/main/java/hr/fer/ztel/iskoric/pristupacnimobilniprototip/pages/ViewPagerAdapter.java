package hr.fer.ztel.iskoric.pristupacnimobilniprototip.pages;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import hr.fer.ztel.iskoric.pristupacnimobilniprototip.pages.fragments.FragmentInfo;
import hr.fer.ztel.iskoric.pristupacnimobilniprototip.pages.fragments.FragmentKontakti;
import hr.fer.ztel.iskoric.pristupacnimobilniprototip.pages.fragments.FragmentMladi;
import hr.fer.ztel.iskoric.pristupacnimobilniprototip.pages.fragments.FragmentOSI;
import hr.fer.ztel.iskoric.pristupacnimobilniprototip.pages.fragments.FragmentPocetna;
import hr.fer.ztel.iskoric.pristupacnimobilniprototip.pages.fragments.FragmentSeniori;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @Override
    @NonNull
    public Fragment getItem(int position) {
        if (position == PageEnum.POCETNA.ordinal()) {
            return new FragmentPocetna();

        } else if (position == PageEnum.MLADI.ordinal()) {
            return new FragmentMladi();

        } else if (position == PageEnum.OSI.ordinal()) {
            return new FragmentOSI();

        } else if (position == PageEnum.SENIORI.ordinal()) {
            return new FragmentSeniori();

        } else if (position == PageEnum.KONTAKTI.ordinal()) {
            return new FragmentKontakti();

        } else {
            return new FragmentInfo();
        }
    }

    @Override
    public int getCount() {
        return 6;
    }
}
