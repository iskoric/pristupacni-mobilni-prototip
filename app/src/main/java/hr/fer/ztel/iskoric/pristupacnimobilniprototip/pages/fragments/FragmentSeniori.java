package hr.fer.ztel.iskoric.pristupacnimobilniprototip.pages.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import hr.fer.ztel.iskoric.pristupacnimobilniprototip.MainActivity;
import hr.fer.ztel.iskoric.pristupacnimobilniprototip.R;

public class FragmentSeniori extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.seniori_layout, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) getActivity()).update.updateFragment(getView());
    }
}

