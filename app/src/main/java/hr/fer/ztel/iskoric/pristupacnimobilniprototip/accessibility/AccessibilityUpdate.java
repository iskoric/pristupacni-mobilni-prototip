package hr.fer.ztel.iskoric.pristupacnimobilniprototip.accessibility;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.TextViewCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import hr.fer.ztel.iskoric.pristupacnimobilniprototip.R;

public class AccessibilityUpdate {

    public final Context context;

    public final AccessibilityPreferences preferences;

    public AccessibilityUpdate(AccessibilityPreferences preferences) {
        this.context = preferences.context;
        this.preferences = preferences;
    }

    public void updateMain(View view) {
        if (view instanceof Toolbar) {
            updateStatusBar();
            updateToolbar((Toolbar) view);

        } else if (view instanceof DrawerLayout) {
            updateDrawerLayout((DrawerLayout) view);

        } else if (view instanceof ImageView) {
            updateImageView((ImageView) view);

        } else if (view instanceof ViewPager) {
            updateViewPager((ViewPager) view);

        } else if (view instanceof TabLayout) {
            updatePageIndicator(((TabLayout) view));

        } else if (view instanceof NavigationView) {
            if (view.getId() == R.id.navigation_view) {
                updateNavigationView((NavigationView) view);
            } else if (view.getId() == R.id.accessibility_view) {
                updateAccessibilityView((NavigationView) view);
            }

        } else if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                updateMain(viewGroup.getChildAt(i));
            }
        }
    }

    private void updateStatusBar() {
        Window window = ((Activity) context).getWindow();
        switch (preferences.getContrast()) {
            case BLACK:
                window.setStatusBarColor(getColor(R.color.statusBarColorContrastBlack));
                break;
            case RED:
                window.setStatusBarColor(getColor(R.color.statusBarColorContrastRed));
                break;
            case GREEN:
                window.setStatusBarColor(getColor(R.color.statusBarColorContrastGreen));
                break;
            case BLUE:
                window.setStatusBarColor(getColor(R.color.statusBarColorContrastBlue));
                break;
            default:
                if (preferences.getUseNightMode()) {
                    window.setStatusBarColor(getColor(R.color.statusBarColorDark));
                } else {
                    window.setStatusBarColor(getColor(R.color.statusBarColor));
                }
                break;
        }
    }

    private void updateToolbar(Toolbar toolbar) {
        switch (preferences.getContrast()) {
            case BLACK:
                toolbar.setBackgroundColor(getColor(R.color.contrastBlackBackground));
                updateToolbarContent(toolbar, getColor(R.color.contrastBlackForeground));
                break;
            case RED:
                toolbar.setBackgroundColor(getColor(R.color.contrastRedBackground));
                updateToolbarContent(toolbar, getColor(R.color.contrastRedForeground));
                break;
            case GREEN:
                toolbar.setBackgroundColor(getColor(R.color.contrastGreenBackground));
                updateToolbarContent(toolbar, getColor(R.color.contrastGreenForeground));
                break;
            case BLUE:
                toolbar.setBackgroundColor(getColor(R.color.contrastBlueBackground));
                updateToolbarContent(toolbar, getColor(R.color.contrastBlueForeground));
                break;
            default:
                if (preferences.getUseNightMode()) {
                    toolbar.setBackgroundColor(getColor(R.color.colorPrimaryDark));
                    updateToolbarContent(toolbar, getColor(R.color.textColorPrimaryDark));
                } else {
                    toolbar.setBackgroundColor(getColor(R.color.colorPrimary));
                    updateToolbarContent(toolbar, getColor(R.color.textColorPrimary));
                }
                break;
        }
    }

    private void updateToolbarContent(View view, int color) {
        if (view instanceof ImageButton) {
            ((ImageButton) view).setColorFilter(color);

        } else if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                updateToolbarContent(viewGroup.getChildAt(i), color);
            }
        }
    }

    private void updateDrawerLayout(DrawerLayout drawerLayout) {
        switch (preferences.getContrast()) {
            case BLACK:
                drawerLayout.setBackgroundColor(getColor(R.color.contrastBlackBackground));
                break;
            case RED:
                drawerLayout.setBackgroundColor(getColor(R.color.contrastRedBackground));
                break;
            case GREEN:
                drawerLayout.setBackgroundColor(getColor(R.color.contrastGreenBackground));
                break;
            case BLUE:
                drawerLayout.setBackgroundColor(getColor(R.color.contrastBlueBackground));
                break;
            default:
                if (preferences.getUseNightMode()) {
                    drawerLayout.setBackgroundColor(getColor(R.color.windowBackgroundDark));
                } else {
                    drawerLayout.setBackgroundColor(getColor(R.color.windowBackground));
                }
                break;
        }

        for (int i = 0; i < drawerLayout.getChildCount(); i++) {
            updateMain(drawerLayout.getChildAt(i));
        }
    }

    private void updateImageView(ImageView imageView) {
        if (preferences.getUseGrayScale()) {
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);
            ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(matrix);
            imageView.setColorFilter(colorFilter);

        } else {
            imageView.clearColorFilter();
        }

        switch (preferences.getContrast()) {
            case BLACK:
                imageView.setForeground(new ColorDrawable(getColor(R.color.contrastBlackForeground)));
                break;
            case RED:
                imageView.setForeground(new ColorDrawable(getColor(R.color.contrastRedForeground)));
                break;
            case GREEN:
                imageView.setForeground(new ColorDrawable(getColor(R.color.contrastGreenForeground)));
                break;
            case BLUE:
                imageView.setForeground(new ColorDrawable(getColor(R.color.contrastBlueForeground)));
                break;
            default:
                imageView.setForeground(null);
                break;
        }
    }

    private void updateViewPager(ViewPager viewPager) {
        for (int i = 0; i < viewPager.getChildCount(); i++) {
            updateFragment(viewPager.getChildAt(i));
        }
    }

    public void updateFragment(View view) {
        if (view instanceof TextView) {
            updatePageTitle((TextView) view);

        } else if (view instanceof CardView) {
            updateCardView((CardView) view);

        } else if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                updateFragment(viewGroup.getChildAt(i));
            }
        }
    }

    private void updatePageTitle(TextView textView) {
        switch (preferences.getContrast()) {
            case BLACK:
                textView.setTextColor(getColor(R.color.contrastBlackBackground));
                textView.setShadowLayer(0, 0, 0, Color.TRANSPARENT);
                break;
            case RED:
                textView.setTextColor(getColor(R.color.contrastRedBackground));
                textView.setShadowLayer(0, 0, 0, Color.TRANSPARENT);
                break;
            case GREEN:
                textView.setTextColor(getColor(R.color.contrastGreenBackground));
                textView.setShadowLayer(0, 0, 0, Color.TRANSPARENT);
                break;
            case BLUE:
                textView.setTextColor(getColor(R.color.contrastBlueBackground));
                textView.setShadowLayer(0, 0, 0, Color.TRANSPARENT);
                break;
            default:
                textView.setTextColor(getColor(R.color.textColorPrimaryDark));
                textView.setShadowLayer(4, 0, 0,
                        getColor(R.color.windowBackgroundDark)
                );
                break;
        }
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, preferences.getFontSizeXL());
        textView.setTypeface(
                preferences.getUseDyslexic() ? getOpenDyslexicTypeface() : Typeface.DEFAULT,
                Typeface.BOLD
        );
    }

    private void updateCardView(CardView cardView) {
        switch (preferences.getContrast()) {
            case BLACK:
                cardView.setBackgroundColor(getColor(R.color.contrastBlackBackground));
                updateCardViewContent(cardView,
                        getColor(R.color.contrastBlackForeground),
                        getColor(R.color.contrastBlackForeground),
                        getColor(R.color.contrastBlackForeground)
                );
                break;
            case RED:
                cardView.setBackgroundColor(getColor(R.color.contrastRedBackground));
                updateCardViewContent(cardView,
                        getColor(R.color.contrastRedForeground),
                        getColor(R.color.contrastRedForeground),
                        getColor(R.color.contrastRedForeground)
                );
                break;
            case GREEN:
                cardView.setBackgroundColor(getColor(R.color.contrastGreenBackground));
                updateCardViewContent(cardView,
                        getColor(R.color.contrastGreenForeground),
                        getColor(R.color.contrastGreenForeground),
                        getColor(R.color.contrastGreenForeground)
                );
                break;
            case BLUE:
                cardView.setBackgroundColor(getColor(R.color.contrastBlueBackground));
                updateCardViewContent(cardView,
                        getColor(R.color.contrastBlueForeground),
                        getColor(R.color.contrastBlueForeground),
                        getColor(R.color.contrastBlueForeground)
                );
                break;
            default:
                if (preferences.getUseNightMode()) {
                    cardView.setBackgroundColor(getColor(R.color.cardBackgroundDark));
                    updateCardViewContent(cardView,
                            getColor(R.color.textColorSecondaryDark),
                            getColor(R.color.textColorTertiaryDark),
                            getColor(R.color.colorAccentDark)
                    );
                } else {
                    cardView.setBackgroundColor(getColor(R.color.cardBackground));
                    updateCardViewContent(cardView,
                            getColor(R.color.textColorSecondary),
                            getColor(R.color.textColorTertiary),
                            getColor(R.color.colorAccent)
                    );
                }
                break;
        }
    }

    private void updateCardViewContent(View view, int titleColor, int textColor, int linkColor) {
        if (view instanceof TextView) {
            String name = getName(view);
            if (name.startsWith("app_title_")) {
                ((TextView) view).setTextColor(
                        preferences.getContrast() == ContrastEnum.NONE ?
                                getColor(R.color.textColorAppTitle) : titleColor
                );
                ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_SP, preferences.getFontSizeL());
                ((TextView) view).setTypeface(
                        preferences.getUseDyslexic() ? getOpenDyslexicTypeface() : Typeface.DEFAULT,
                        Typeface.BOLD
                );

            } else if (name.startsWith("title_")) {
                ((TextView) view).setTextColor(titleColor);
                ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_SP, preferences.getFontSizeL());
                ((TextView) view).setTypeface(
                        preferences.getUseDyslexic() ? getOpenDyslexicTypeface() : Typeface.DEFAULT,
                        Typeface.BOLD
                );

            } else if (name.startsWith("text_l_")) {
                ((TextView) view).setTextColor(textColor);
                ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_SP, preferences.getFontSizeL());
                ((TextView) view).setTypeface(
                        preferences.getUseDyslexic() ? getOpenDyslexicTypeface() : Typeface.DEFAULT
                );

            } else if (name.startsWith("text_m_")) {
                ((TextView) view).setTextColor(textColor);
                ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_SP, preferences.getFontSizeM());
                ((TextView) view).setTypeface(
                        preferences.getUseDyslexic() ? getOpenDyslexicTypeface() : Typeface.DEFAULT
                );

            } else {
                ((TextView) view).setTextColor(textColor);
                ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_SP, preferences.getFontSizeS());
                ((TextView) view).setTypeface(
                        preferences.getUseDyslexic() ? getOpenDyslexicTypeface() : Typeface.DEFAULT
                );
            }

            ((TextView) view).setLineSpacing(6, preferences.getUseDyslexic() ? 1.4f : 1);

            SpannableString content = new SpannableString(((TextView) view).getText());
            if (preferences.getUnderlineLinks()) {
                URLSpan[] linkSpans = content.getSpans(0, content.length(), URLSpan.class);
                for (URLSpan span : linkSpans) {
                    int start = content.getSpanStart(span);
                    int end = content.getSpanEnd(span);
                    content.setSpan(new UnderlineSpan(), start, end, 0);
                }
                ((TextView) view).setText(content);
            } else {
                UnderlineSpan[] underlineSpans = content.getSpans(0, content.length(), UnderlineSpan.class);
                for (UnderlineSpan span : underlineSpans) {
                    content.removeSpan(span);
                }
                ((TextView) view).setText(content);
                ((TextView) view).setLinkTextColor(linkColor);
            }

            int highlightBackgroundColor = ContextCompat.getColor(view.getContext(), R.color.colorHighlightBackground);
            int highlightForegroundColor = ContextCompat.getColor(view.getContext(), R.color.colorHighlightForeground);
            content = new SpannableString(((TextView) view).getText());
            if (preferences.getHighlightLinks()) {
                URLSpan[] linkSpans = content.getSpans(0, content.length(), URLSpan.class);
                for (URLSpan span : linkSpans) {
                    int start = content.getSpanStart(span);
                    int end = content.getSpanEnd(span);
                    content.setSpan(new BackgroundColorSpan(highlightBackgroundColor), start, end, 0);
                }
                ((TextView) view).setText(content);
                ((TextView) view).setLinkTextColor(highlightForegroundColor);
            } else {
                BackgroundColorSpan[] highlightSpans = content.getSpans(0, content.length(), BackgroundColorSpan.class);
                for (BackgroundColorSpan span : highlightSpans) {
                    if (span.getBackgroundColor() == highlightBackgroundColor) {
                        content.removeSpan(span);
                    }
                }
                ((TextView) view).setText(content);
                ((TextView) view).setLinkTextColor(linkColor);
            }

        } else if (view instanceof ImageView) {
            if (view.getId() == R.id.image_fer) {
                ((ImageView) view).setColorFilter(titleColor);

            } else if (preferences.getUseGrayScale()) {
                ColorMatrix matrix = new ColorMatrix();
                matrix.setSaturation(0);
                ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(matrix);
                ((ImageView) view).setColorFilter(colorFilter);

            } else {
                ((ImageView) view).clearColorFilter();
            }

        } else if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                updateCardViewContent(viewGroup.getChildAt(i), titleColor, textColor, linkColor);
            }
        }
    }

    private void updatePageIndicator(TabLayout pageIndicator) {
        switch (preferences.getContrast()) {
            case BLACK:
                pageIndicator.setSelectedTabIndicatorColor(getColor(R.color.contrastBlackForeground));
                break;
            case RED:
                pageIndicator.setSelectedTabIndicatorColor(getColor(R.color.contrastRedBackground));
                break;
            case GREEN:
                pageIndicator.setSelectedTabIndicatorColor(getColor(R.color.contrastGreenBackground));
                break;
            case BLUE:
                pageIndicator.setSelectedTabIndicatorColor(getColor(R.color.contrastBlueForeground));
                break;
            default:
                pageIndicator.setSelectedTabIndicatorColor(getColor(R.color.colorAccentDark));
                break;
        }
    }

    private void updateNavigationView(NavigationView navigationView) {
        switch (preferences.getContrast()) {
            case BLACK:
                navigationView.setBackgroundColor(getColor(R.color.contrastBlackBackground));
                updateNavigationViewContent(navigationView, getColor(R.color.contrastBlackForeground));
                break;
            case RED:
                navigationView.setBackgroundColor(getColor(R.color.contrastRedBackground));
                updateNavigationViewContent(navigationView, getColor(R.color.contrastRedForeground));
                break;
            case GREEN:
                navigationView.setBackgroundColor(getColor(R.color.contrastGreenBackground));
                updateNavigationViewContent(navigationView, getColor(R.color.contrastGreenForeground));
                break;
            case BLUE:
                navigationView.setBackgroundColor(getColor(R.color.contrastBlueBackground));
                updateNavigationViewContent(navigationView, getColor(R.color.contrastBlueForeground));
                break;
            default:
                if (preferences.getUseNightMode()) {
                    navigationView.setBackgroundColor(getColor(R.color.windowBackgroundDark));
                    updateNavigationViewContent(navigationView, getColor(R.color.textColorPrimaryDark));
                } else {
                    navigationView.setBackgroundColor(getColor(R.color.cardBackground));
                    updateNavigationViewContent(navigationView, getColor(R.color.colorSecondary));
                }
                break;
        }
    }

    private void updateNavigationViewContent(View view, int color) {
        if (view instanceof TextView) {
            if (preferences.getUnderlineLinks()) {
                SpannableString content = new SpannableString(((TextView) view).getText());
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                ((TextView) view).setText(content);
            } else {
                ((TextView) view).setText(((TextView) view).getText().toString());
            }

            if (preferences.getHighlightLinks()) {
                int highlightBackgroundColor = ContextCompat.getColor(view.getContext(), R.color.colorHighlightBackground);
                int highlightForegroundColor = ContextCompat.getColor(view.getContext(), R.color.colorHighlightForeground);
                view.setBackgroundColor(highlightBackgroundColor);
                ((TextView) view).setTextColor(highlightForegroundColor);
            } else {
                Drawable background = getAttributeDrawable(R.attr.selectableItemBackground);
                view.setBackground(background);
                ((TextView) view).setTextColor(color);
            }

            ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_SP, preferences.getFontSizeL());
            ((TextView) view).setTypeface(
                    preferences.getUseDyslexic() ? getOpenDyslexicTypeface() : Typeface.DEFAULT,
                    Typeface.BOLD
            );
            ((TextView) view).setLineSpacing(6, preferences.getUseDyslexic() ? 1.4f : 1);

        } else if (view.getId() == R.id.menu_header) {
            updateMenuHeader(view);

        } else if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                updateNavigationViewContent(viewGroup.getChildAt(i), color);
            }
        }
    }

    private void updateMenuHeader(View menuHeader) {
        switch (preferences.getContrast()) {
            case BLACK:
                menuHeader.setBackgroundColor(getColor(R.color.contrastBlackBackground));
                updateMenuHeaderContent(menuHeader, getColor(R.color.contrastBlackForeground));
                break;
            case RED:
                menuHeader.setBackgroundColor(getColor(R.color.contrastRedBackground));
                updateMenuHeaderContent(menuHeader, getColor(R.color.contrastRedForeground));
                break;
            case GREEN:
                menuHeader.setBackgroundColor(getColor(R.color.contrastGreenBackground));
                updateMenuHeaderContent(menuHeader, getColor(R.color.contrastGreenForeground));
                break;
            case BLUE:
                menuHeader.setBackgroundColor(getColor(R.color.contrastBlueBackground));
                updateMenuHeaderContent(menuHeader, getColor(R.color.contrastBlueForeground));
                break;
            default:
                if (preferences.getUseNightMode()) {
                    menuHeader.setBackgroundColor(getColor(R.color.cardBackgroundDark));
                } else {
                    menuHeader.setBackgroundColor(getColor(R.color.textColorTertiary));
                }
                updateMenuHeaderContent(menuHeader, getColor(R.color.textColorPrimaryDark));
                break;
        }
    }

    private void updateMenuHeaderContent(View view, int color) {
        if (view instanceof TextView) {
            if (view.getId() == R.id.app_title) {
                ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_SP, preferences.getFontSizeXL());
                ((TextView) view).setTypeface(
                        preferences.getUseDyslexic() ? getOpenDyslexicTypeface() : Typeface.DEFAULT,
                        Typeface.BOLD
                );

            } else if (view.getId() == R.id.menu_header_text) {
                ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_SP, preferences.getFontSizeXS());
                ((TextView) view).setTypeface(
                        preferences.getUseDyslexic() ? getOpenDyslexicTypeface() : Typeface.DEFAULT,
                        Typeface.ITALIC
                );
            }

            ((TextView) view).setTextColor(color);
            ((TextView) view).setLineSpacing(6, preferences.getUseDyslexic() ? 1.4f : 1);

        } else if (view instanceof ImageView) {
            ((ImageView) view).setColorFilter(color);

        } else if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                updateMenuHeaderContent(viewGroup.getChildAt(i), color);
            }
        }
    }

    private void updateAccessibilityView(NavigationView accessibilityView) {
        switch (preferences.getContrast()) {
            case BLACK:
                accessibilityView.setBackgroundColor(getColor(R.color.contrastBlackBackground));
                updateAccessibilityViewContent(accessibilityView,
                        getColor(R.color.contrastBlackBackground),
                        R.drawable.btn_toggle_background_black,
                        getColor(R.color.contrastBlackForeground),
                        getDrawable(R.drawable.ic_contrast_black_black),
                        getDrawable(R.drawable.ic_contrast_red_black),
                        getDrawable(R.drawable.ic_contrast_green_black),
                        getDrawable(R.drawable.ic_contrast_blue_black)
                );
                break;
            case RED:
                accessibilityView.setBackgroundColor(getColor(R.color.contrastRedBackground));
                updateAccessibilityViewContent(accessibilityView,
                        getColor(R.color.contrastRedBackground),
                        R.drawable.btn_toggle_background_red,
                        getColor(R.color.contrastRedForeground),
                        getDrawable(R.drawable.ic_contrast_black_red),
                        getDrawable(R.drawable.ic_contrast_red_red),
                        getDrawable(R.drawable.ic_contrast_green_red),
                        getDrawable(R.drawable.ic_contrast_blue_red)
                );
                break;
            case GREEN:
                accessibilityView.setBackgroundColor(getColor(R.color.contrastGreenBackground));
                updateAccessibilityViewContent(accessibilityView,
                        getColor(R.color.contrastGreenBackground),
                        R.drawable.btn_toggle_background_green,
                        getColor(R.color.contrastGreenForeground),
                        getDrawable(R.drawable.ic_contrast_black_green),
                        getDrawable(R.drawable.ic_contrast_red_green),
                        getDrawable(R.drawable.ic_contrast_green_green),
                        getDrawable(R.drawable.ic_contrast_blue_green)
                );
                break;
            case BLUE:
                accessibilityView.setBackgroundColor(getColor(R.color.contrastBlueBackground));
                updateAccessibilityViewContent(accessibilityView,
                        getColor(R.color.contrastBlueBackground),
                        R.drawable.btn_toggle_background_blue,
                        getColor(R.color.contrastBlueForeground),
                        getDrawable(R.drawable.ic_contrast_black_blue),
                        getDrawable(R.drawable.ic_contrast_red_blue),
                        getDrawable(R.drawable.ic_contrast_green_blue),
                        getDrawable(R.drawable.ic_contrast_blue_blue)
                );
                break;
            default:
                if (preferences.getUseNightMode()) {
                    accessibilityView.setBackgroundColor(getColor(R.color.windowBackgroundDark));
                    updateAccessibilityViewContent(accessibilityView,
                            getColor(R.color.cardBackgroundDark),
                            R.drawable.btn_toggle_background_dark,
                            getColor(R.color.textColorSecondaryDark),
                            getDrawable(R.drawable.ic_contrast_black_dark),
                            getDrawable(R.drawable.ic_contrast_red_dark),
                            getDrawable(R.drawable.ic_contrast_green_dark),
                            getDrawable(R.drawable.ic_contrast_blue_dark)
                    );
                } else {
                    accessibilityView.setBackgroundColor(getColor(R.color.windowBackground));
                    updateAccessibilityViewContent(accessibilityView,
                            getColor(R.color.cardBackground),
                            R.drawable.btn_toggle_background,
                            getColor(R.color.textColorSecondary),
                            getDrawable(R.drawable.ic_contrast_black),
                            getDrawable(R.drawable.ic_contrast_red),
                            getDrawable(R.drawable.ic_contrast_green),
                            getDrawable(R.drawable.ic_contrast_blue)
                    );
                }
                break;
        }
    }

    private void updateAccessibilityViewContent(View view, int buttonColor, int buttonBackgroundId,
                                                int foregroundColor,
                                                Drawable contrastBlackIcon,
                                                Drawable contrastRedIcon,
                                                Drawable contrastGreenIcon,
                                                Drawable contrastBlueIcon) {
        if (view instanceof Button) {
            if (preferences.getUseDyslexic()) {
                ((Button) view).setTypeface(getOpenDyslexicTypeface());

            } else {
                ((Button) view).setTextAppearance(R.style.TextAppearance_AppCompat_Button);
            }

            if (view instanceof ToggleButton) {
                String name = getName(view);
                switch (name) {
                    case "contrast_black":
                        ((ToggleButton) view).setCompoundDrawablesRelativeWithIntrinsicBounds(null, contrastBlackIcon, null, null);
                        break;
                    case "contrast_red":
                        ((ToggleButton) view).setCompoundDrawablesRelativeWithIntrinsicBounds(null, contrastRedIcon, null, null);
                        break;
                    case "contrast_green":
                        ((ToggleButton) view).setCompoundDrawablesRelativeWithIntrinsicBounds(null, contrastGreenIcon, null, null);
                        break;
                    case "contrast_blue":
                        ((ToggleButton) view).setCompoundDrawablesRelativeWithIntrinsicBounds(null, contrastBlueIcon, null, null);
                        break;
                    default:
                        TextViewCompat.setCompoundDrawableTintList((TextView) view, ColorStateList.valueOf(foregroundColor));
                        break;
                }

            } else {
                view.setBackgroundTintList(ColorStateList.valueOf(buttonColor));
                TextViewCompat.setCompoundDrawableTintList((TextView) view, ColorStateList.valueOf(foregroundColor));
            }

            view.setBackgroundResource(buttonBackgroundId);
            ((Button) view).setTextColor(foregroundColor);
            ((Button) view).setTextSize(TypedValue.COMPLEX_UNIT_SP, preferences.getFontSizeXS());
            ((Button) view).setLineSpacing(6, preferences.getUseDyslexic() ? 1.4f : 1);

        } else if (view instanceof TextView) {
            if (preferences.getUseDyslexic()) {
                ((TextView) view).setTypeface(getOpenDyslexicTypeface());

            } else {
                ((TextView) view).setTextAppearance(R.style.TextAppearance_AppCompat_Button);
            }

            ((TextView) view).setTextColor(foregroundColor);
            ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_SP, preferences.getFontSizeXS());
            ((TextView) view).setLineSpacing(6, preferences.getUseDyslexic() ? 1.4f : 1);

        } else if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                updateAccessibilityViewContent(viewGroup.getChildAt(i), buttonColor, buttonBackgroundId,
                        foregroundColor, contrastBlackIcon, contrastRedIcon, contrastGreenIcon, contrastBlueIcon);
            }
        }
    }

    private String getName(View view) {
        String name = context.getResources().getResourceEntryName(view.getId());
        return name != null ? name : "";
    }

    private int getColor(int colorId) {
        return ContextCompat.getColor(context, colorId);
    }

    private Drawable getDrawable(int drawableId) {
        return ContextCompat.getDrawable(context, drawableId);
    }

    private Typeface getOpenDyslexicTypeface() {
        return Typeface.createFromAsset(context.getAssets(), "OpenDyslexicAlta-Regular.ttf");
    }

    @Nullable
    private Drawable getAttributeDrawable(int attributeId) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(attributeId, typedValue, true);
        int drawableId = typedValue.resourceId;
        return ContextCompat.getDrawable(context, drawableId);
    }
}
