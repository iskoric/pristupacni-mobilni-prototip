package hr.fer.ztel.iskoric.pristupacnimobilniprototip.accessibility;

public enum ContrastEnum {
    NONE,
    BLACK,
    RED,
    GREEN,
    BLUE
}
