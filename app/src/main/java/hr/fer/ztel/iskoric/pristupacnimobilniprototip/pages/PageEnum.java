package hr.fer.ztel.iskoric.pristupacnimobilniprototip.pages;

import hr.fer.ztel.iskoric.pristupacnimobilniprototip.R;

public enum PageEnum {
    POCETNA(R.string.desc_pocetna),
    MLADI(R.string.desc_mladi),
    OSI(R.string.desc_osi),
    SENIORI(R.string.desc_seniori),
    KONTAKTI(R.string.desc_kontakti),
    INFO(R.string.desc_info);

    private final int descriptionId;

    PageEnum(int descriptionId) {
        this.descriptionId = descriptionId;
    }

    public int getDescriptionId() {
        return descriptionId;
    }
}
